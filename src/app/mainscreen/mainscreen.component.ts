import { Component, OnInit } from '@angular/core';
import { MNCService } from 'src/services/mnc.service';

@Component({
  selector: 'app-mainscreen',
  templateUrl: './mainscreen.component.html',
  styleUrls: ['./mainscreen.component.scss']
})
export class MainscreenComponent implements OnInit {

  show = false;
  car_result: any;

  constructor(public service: MNCService) { }

  ngOnInit(): void {
    // let data = {
    //   'city_id' : 1,
    //   'type': 'web',
    //   'brand_id':16,
    //   'model_id':465
    // }
    this.service.get_variantList().subscribe((res) => {
      if(res.status === 'pass') {
        this.car_result = res.data;
        console.log(this.car_result);
        this.show = false;
      }else {
        this.show = true;
      }
    });
  }

}
