import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';
import { MainscreenComponent } from './mainscreen/mainscreen.component';

const routes: Routes = [  { path: '', redirectTo: '/home', pathMatch: 'full' },
{ path: 'home', component: MainscreenComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }