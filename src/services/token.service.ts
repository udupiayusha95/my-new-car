import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class TokenService {
    constructor() { }

    setToken(token) {
        localStorage.setItem("token", token);
    }

    getToken() {
        let token = localStorage.getItem("token");
        return token;
    }

    clearToken() {
        localStorage.clear();
    }

}