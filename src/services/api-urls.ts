import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class API_urls {

    constructor() { }

    public variant_list = environment.api_url + 'api_mnc/variants_list?city_id=1&type=web&brand_id=16&model_id=465';

}