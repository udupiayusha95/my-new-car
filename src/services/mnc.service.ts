import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenService } from '../services/token.service';
import { map, catchError } from 'rxjs/operators';
import { API_urls } from './api-urls';

@Injectable({
    providedIn: 'root'
})
export class MNCService {
    header;

    constructor(private http: HttpClient, private token: TokenService, private apiUrls: API_urls) {
        this.header = {
            'Content-Type': 'application/json',
            'Authorization': 'Token ' + this.token.getToken()
        }
    }

    get_variantList(): Observable<any> {
        return this.http.get(this.apiUrls.variant_list);
    }

}